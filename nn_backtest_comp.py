from sklearn.neural_network import MLPClassifier
import pandas as pd
import numpy as np
from bitarray import bitarray
import pickle
from multiprocessing import Pool
from datetime import datetime
from math import sqrt
from sklearn.externals import joblib
from sklearn.preprocessing import binarize



start=datetime.now()
print start
def cptf(x):
    if x>0.0:
        return 1
    else:
        return 0

def tanimoto(A,B):
    a=(A&~B).count()
    b=(B&~A).count()
    c=(A&B).count()
    d=(~A&~B).count()
    return float(c)/float(a+b+c)

def quat_h(x):
    if x.minute%10==0:
        return True
    else:
        return False

def bitarray2(x):
    return bitarray(x)

def prediction_basis(x):
    return clf.predict(x)



'''


#liveFPT
output_dataframe=pd.DataFrame(columns=['pair','similarity','fingp_count','next_period_probability','price_at_calc','last_close_for_calc','mean_of_future','std_in_fututure','last_close_in_future','last_date_in_fututre'])
table_init=pd.read_csv('../clean_curr_feed.csv')
output_file=open('backtest_log_10.txt','w')

performance_table=pd.DataFrame(columns=)

live_fingerprints=[]
futures=[]
for day in range(100,4000):
    table=table_init.iloc[day::10][:13]
    pairs=table.columns.values[1:]
    for u in table.columns.values[1:]:
        bits_changes=[]
        for time_step in range(1,9):
            bits_changes=bits_changes+[cptf(chg) for chg in list(table[u].iloc[:9].diff(periods=time_step))][time_step:]
        if bits_changes!=[]:
            live_fingerprints.append(bitarray(bits_changes).tolist())
        else:
            print day
        bits_future=[]
        for time_step in range(1,5):
            bits_future=bits_future+[cptf(chg) for chg in list(table[u].iloc[8:13].diff(periods=time_step))][time_step:]
        if bits_future!=[]:
            futures.append(bitarray(bits_future).tolist()[0])
        else:
            print day

'''
def backtest(output):
    print output
    clf = joblib.load('test_misha_model_big_%d.pkl'%output)
    
    print clf.loss_
    print clf.n_iter_

    live_fingerprints=open('fing10_dat/all_pasts.jkla','r').read().split('\n')[200000:300000]
    live_fingerprints=[bitarray(x.split(',')[-1]).tolist() for x in live_fingerprints]
    futures=open('fing10_dat/all_futures.jkla','r').read().split('\n')[200000:300000]

    futures=np.array([bitarray(x).tolist() for x in futures])
    futures=binarize(futures)[:,output].reshape(len(futures),)


    print clf.predict(live_fingerprints[5]),clf.predict(live_fingerprints[-1])
    print clf.predict_proba(live_fingerprints[5]),clf.predict_proba(live_fingerprints[-1])
    print clf.classes_
    print futures[5],futures[-1]

    data=pd.DataFrame(data={'next_period_probability':[x[1] for x in clf.predict_proba(live_fingerprints)],'predicted':clf.predict(live_fingerprints),'real': futures})

    perfs=[]
    for i in np.arange(0.25,0.61,0.01):
        peisa=data[((data['next_period_probability']>i) & (data['next_period_probability']<=i+0.01))]
        
        if len(peisa)>1:
            perfs.append([float(len(peisa[peisa['predicted']==peisa['real']]))/float(len(peisa)),float(len(peisa))/float(len(data))])
        else:
            perfs.append([np.nan,len(peisa)])
    performance_table['accuracy%d'%output]=np.array(perfs)[:,0]
    performance_table['frequency%d'%output]=np.array(perfs)[:,1]
    print clf.predict(live_fingerprints)[5]
    print clf.score(live_fingerprints,np.array(futures))
    shaha=list(data['predicted'])
    print float(len([futures[x] for x in range(len(futures)) if futures[x]==0]))/float(len(futures))
    print float(len([futures[x] for x in range(len(futures)) if futures[x]==1]))/float(len(futures))
    print np.unique(futures)

performance_table=pd.DataFrame(columns=['accuracy%d' % x for x in range(0,10)]+['frequency%d' % x for x in range(0,10)],index=np.arange(0.25,0.61,0.01))
for output in range(0,10):
    backtest(output)

performance_table.to_csv('performance_of_model.csv')
