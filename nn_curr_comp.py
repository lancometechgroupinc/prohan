from sklearn.neural_network import MLPClassifier
import pandas as pd
import numpy as np
from bitarray import bitarray
import pickle
from multiprocessing import Pool
from datetime import datetime
from math import sqrt
from sklearn.externals import joblib
from sklearn.preprocessing import binarize


fingerprints_dats=open('fing10_dat/all_pasts.jkla','r').read().split('\n')[-200000:-1]
fingerprints_dats=[bitarray(x.split(',')[-1]).tolist() for x in fingerprints_dats]
idens_dats=open('fing10_dat/all_futures.jkla','r').read().split('\n')[-200000:-1]
print len(idens_dats)
print len(fingerprints_dats)
idens_dats_tot=[bitarray(x).tolist() for x in idens_dats]

def train_to_output(output):
    print output
    idens_dats=binarize(idens_dats_tot)[:,output].reshape(len(idens_dats_tot),)
    clf = MLPClassifier(solver='lbfgs',activation='identity',hidden_layer_sizes=(800,800,800,800,800),batch_size='auto', early_stopping=False,learning_rate='constant', max_iter=1000,
                    nesterovs_momentum=True, random_state=1, shuffle=True, validation_fraction=0.1, verbose=False,warm_start=True,tol=1.0e-45,learning_rate_init=1e-35,alpha=0.00001)
    print clf.fit(fingerprints_dats, idens_dats)



    print clf.get_params()
    print clf.loss_
    print clf.classes_
    joblib.dump(clf, '800_nn/test_misha_model_big_%d.pkl'%output)

for i in range(6,10):
    train_to_output(i)
